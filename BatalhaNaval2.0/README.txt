
	BatalhaNaval2.0 � um jogo desenvolvido em java utilizando a IDE NetBeans 8.2.

	O jogo consiste em uma vers�o modificada do jogo de tabuleiro "batalha naval" 
para apenas um jogador.

	Ao abrir o jogo ser� mostrada o menu com os seguintes bot�es:

		> recordes: abre uma tela com as cinco melhores pontua��es obtidas;
		
		> instru��es: abre uma tela com as instru��es do jogo;

		> jogar: abre uma tela com o mapa de jogo. O mapa � escolhido de forma
			 aleat�ria entre os mapas disponiveis	  
		

	Na tela de jogo � dispon�vel quatro tipos de armas ao jogador, podendo o jogado 
atacar apenas uma casa, uma linha, uma coluna, ou uma area tr�s por tr�s.

	O jogo termina quando todos os navios s�o destru�dos ou quando acabam as jogadas
disponiveis.

	Ao fim do jogo � aberta uma tela onde o jogador poder� digitar seu nome, abrir 
um novo jogo ou ir para o menu principal. Se o jogador obtiver uma pontua��o acima de
pelo menos uma das pontua��es registradas, ser� salvo seu novo record.

Obs.: a tela de fim de jogo n�o poder� ser simplesmente fechada, para isto, ou o jogador
      inicia uma nova partida ou vai para o menu principal. Para fechar a aplica��o basta
      que o jogador clique no icone padr�o de fechar("X").

		 