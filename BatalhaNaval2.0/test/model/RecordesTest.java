/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package model;

import org.junit.After;
import org.junit.AfterClass;
import org.junit.Before;
import org.junit.BeforeClass;
import org.junit.Test;
import static org.junit.Assert.*;

/**
 *
 * @author lieve
 */
public class RecordesTest {
    
    public RecordesTest() {
    }
    
    @BeforeClass
    public static void setUpClass() {
    }
    
    @AfterClass
    public static void tearDownClass() {
    }
    
    @Before
    public void setUp() {
    }
    
    @After
    public void tearDown() {
    }

    /**
     * Test of salvarRecorde method, of class Recordes.
     */
    @Test
    public void testSalvarRecorde() {
        System.out.println("salvarRecorde");
        String jogador4 = "";
        int pontos4 = 0;
        Recordes instance = new Recordes();
        instance.salvarRecorde(jogador4, pontos4);
    }

    @Test
    public void testGetJogador() {
        System.out.println("getJogador");
        Recordes instance = new Recordes();
        String[] expResult = new String[1];
        expResult[0] = "Jogador";
        String[] result = new String[1];
        result[0] = instance.getJogador()[0];
        assertArrayEquals(expResult, result);
    }

    @Test
    public void testSetJogador() {
        System.out.println("setJogador");
        String[] jogador = null;
        Recordes instance = new Recordes();
        instance.setJogador(jogador);
    }

    @Test
    public void testGetPontos() {
        System.out.println("getPontos");
        Recordes instance = new Recordes();
        int[] expResult = new int[1];
        expResult[0] = 0;
        int[] result = new int[1];
        result[0] = instance.getPontos()[0];
        assertArrayEquals(expResult, result);
    }

    @Test
    public void testSetPontos() {
        System.out.println("setPontos");
        int[] pontos = null;
        Recordes instance = new Recordes();
        instance.setPontos(pontos);
    }
    
}
