
package controller;

import java.util.logging.Level;
import java.util.logging.Logger;
import model.DesenhoExplosao;

public class RenderizarExplosao extends Thread {
    private final DesenhoExplosao desenhoExplosao;
    private final boolean running = true; 

    public RenderizarExplosao(DesenhoExplosao desenhoExplosao) {
            this.desenhoExplosao = desenhoExplosao;
    }
            
    @Override
    public void run() {
        while(running) {
            try {
                Thread.sleep(50);
            } catch (InterruptedException ex) {
                Logger.getLogger(RenderizarExplosao.class.getName()).log(Level.SEVERE, null, ex);
            }
            desenhoExplosao.paint(desenhoExplosao.getGraphics());
        }
    }
}
