
package controller;

import java.util.logging.Level;
import java.util.logging.Logger;
import model.DesenhoOceano;

public class RenderizarOceano extends Thread {
    private final DesenhoOceano desenhoOceano;
    private final boolean running = true; 

    public RenderizarOceano(DesenhoOceano desenhoOceano) {
            this.desenhoOceano = desenhoOceano;
    }
            
    @Override
    public void run() {
        while(running) {
            try {
                Thread.sleep(100);
            } catch (InterruptedException ex) {
                Logger.getLogger(RenderizarOceano.class.getName()).log(Level.SEVERE, null, ex);
            }
            desenhoOceano.paint(desenhoOceano.getGraphics());
        }
    }
}
