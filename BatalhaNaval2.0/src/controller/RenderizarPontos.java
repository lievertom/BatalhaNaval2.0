package controller;

import java.util.logging.Level;
import java.util.logging.Logger;
import model.Pontos;
import model.DesenhoNavios;

public class RenderizarPontos extends Thread {
    private final Pontos pontos;
    private final boolean running = true; 

    public RenderizarPontos(Pontos pontos) {
        this.pontos = pontos;
    }
            
    @Override
    public void run() {
        while(running) {
            try {
                Thread.sleep(1000);
            } catch (InterruptedException ex) {
                Logger.getLogger(RenderizarExplosao.class.getName()).log(Level.SEVERE, null, ex);
            }
            pontos.setText(String.valueOf(DesenhoNavios.pontos));
        }
    }
}