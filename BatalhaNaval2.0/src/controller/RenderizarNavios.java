
package controller;

import java.util.logging.Level;
import java.util.logging.Logger;
import model.DesenhoNavios;

public class RenderizarNavios extends Thread {
    private final DesenhoNavios desenhoNavios;
    private final boolean running = true; 

    public RenderizarNavios(DesenhoNavios desenhoNavios) {
            this.desenhoNavios = desenhoNavios;
    }
            
    @Override
    public void run() {
        while(running) {
            try {
                Thread.sleep(60);
            } catch (InterruptedException ex) {
                Logger.getLogger(RenderizarExplosao.class.getName()).log(Level.SEVERE, null, ex);
            }
            desenhoNavios.paint(desenhoNavios.getGraphics());
        }
    }
}

