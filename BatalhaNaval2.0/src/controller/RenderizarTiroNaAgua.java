package controller;

import java.util.logging.Level;
import java.util.logging.Logger;
import model.DesenhoTiroNaAgua;

public class RenderizarTiroNaAgua extends Thread {
    private final DesenhoTiroNaAgua desenhoTiroNaAgua;
    private final boolean running = true; 

    public RenderizarTiroNaAgua(DesenhoTiroNaAgua desenhoTiroNaAgua) {
            this.desenhoTiroNaAgua = desenhoTiroNaAgua;
    }
            
    @Override
    public void run() {
        while(running) {
            try {
                Thread.sleep(500);
            } catch (InterruptedException ex) {
                Logger.getLogger(RenderizarExplosao.class.getName()).log(Level.SEVERE, null, ex);
            }
            desenhoTiroNaAgua.paint(desenhoTiroNaAgua.getGraphics());
        }
    }
}
