
package model;

import java.awt.Image;
import java.io.IOException;
import java.util.logging.Level;
import java.util.logging.Logger;

public class TiroNaAgua {
    public Image tiroNaAgua ;
    
    public TiroNaAgua(int contadorAnimacaoTiriNaAgua) {         
        try {
            tiroNaAgua = javax.imageio.ImageIO.read(new java.net.URL(getClass().getResource("/imagens/tironaagua/bubble"+contadorAnimacaoTiriNaAgua+".png"), "bubble"+contadorAnimacaoTiriNaAgua+".png"));
        } catch (IOException ex) {
            Logger.getLogger(Oceano.class.getName()).log(Level.SEVERE, null, ex);
        }       
    }

    public Image getTiroNaAgua() {
        return tiroNaAgua;
    }    
}