
package model;

import java.awt.Graphics;
import javax.swing.JLabel;

public class DesenhoOceano extends JLabel {
    public static final int ALTURA_CELULA = 70;
    public static final int LARGURA_CELULA = 70;
 
    private int contadorAnimacaoOceano = 0;

    @Override
    public void paint(Graphics celula) {
        
        
        contadorAnimacaoOceano++;
        if (contadorAnimacaoOceano == 22) {
            contadorAnimacaoOceano = 1;
        } 
        
        Oceano oceano = new Oceano(contadorAnimacaoOceano);
        
        for(int i = 0; i < Mapa.LINHAS/2; i++) {
            for(int j = 0; j < Mapa.COLUNAS/4; j++) {
                celula.drawImage(oceano.getOceano(), i*LARGURA_CELULA*2, j*ALTURA_CELULA*4, LARGURA_CELULA*2, ALTURA_CELULA*4, null);
            }
        }                
    }  
}
