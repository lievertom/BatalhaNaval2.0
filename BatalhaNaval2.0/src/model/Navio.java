
package model;

import java.awt.Image;
import java.io.IOException;
import java.util.logging.Level;
import java.util.logging.Logger;

public class Navio {
    
    public static Image NAVIO;
    
    public Navio(String nomeNavio) {
        try {
            NAVIO = javax.imageio.ImageIO.read(new java.net.URL(getClass().getResource("/imagens/navios/Ship"+nomeNavio+"Hull.png"), "Ship"+nomeNavio+"Hull.png"));
        } catch (IOException ex) {
            Logger.getLogger(Navio.class.getName()).log(Level.SEVERE, null, ex);
        }
    }
}
