
package model;

import java.io.BufferedReader;
import java.io.FileReader;
import java.io.IOException;
import java.util.logging.Level;
import java.util.logging.Logger;

public class Mapa {
    
    public static int LINHAS = 8;
    public static int COLUNAS = 8;
    public static int NUMERO_DE_NAVIOS = 5;
    public static int NUMERO_DE_MAPAS = 10;
    public static boolean [][] MATRIZ_ESCOLHA;
    
    private String caminho;
    private FileReader arquivo;
    private BufferedReader lerArquivo;
    private String lerLinha;
    private String[] posicao;
    private int linha[];
    private int coluna[]; 
    private int matrizMapa[][];
    private int[][] matrizPosicaoNavios;
    
    public Mapa(int numeroMapa) {
        try {
            caminho = "src/arquivostxt/map_"+numeroMapa+".txt";
            arquivo = new FileReader(caminho);
            lerArquivo = new BufferedReader(arquivo);
            linha = new int[NUMERO_DE_NAVIOS];
            coluna = new int[NUMERO_DE_NAVIOS];
            matrizMapa = new int[LINHAS][COLUNAS];
            MATRIZ_ESCOLHA = new boolean[Mapa.LINHAS][Mapa.COLUNAS];
            
            for (int i = 0; i < NUMERO_DE_NAVIOS; i++){
                lerLinha = lerArquivo.readLine();
                posicao = lerLinha.split("");
                linha[i] =  Integer.valueOf(posicao[0]);
                coluna[i] =  Integer.valueOf(posicao[1]);
            }
            for (int i = 0; i < LINHAS; i++){
                lerLinha = lerArquivo.readLine();
                posicao = lerLinha.split("");
                for (int j =0; j < COLUNAS; j++){
                    matrizMapa[i][j] = Integer.valueOf(posicao[j]);
                    MATRIZ_ESCOLHA[i][j] = true;
                }
            }
            
        } catch (IOException ex) {
            Logger.getLogger(Mapa.class.getName()).log(Level.SEVERE, null, ex);
        }   
    }

    public int[][] getMatrizMapa() {
        return matrizMapa;
    }
    public int[][] getMatrizPosicaoNavios() {
        return matrizPosicaoNavios;
    }

    public int[] getLinha() {
        return linha;
    }

    public int[] getColuna() {
        return coluna;
    }
    
}
