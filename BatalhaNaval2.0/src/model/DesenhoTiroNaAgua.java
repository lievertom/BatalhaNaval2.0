
package model;

import java.awt.Graphics;

public class DesenhoTiroNaAgua extends DesenhoNavios {
    public static final int ALTURA_CELULA = 70;
    public static final int LARGURA_CELULA = 70;
    private int [][] matrizTiro = new int[Mapa.LINHAS][Mapa.COLUNAS];
 
    private int contadorAnimacaoTiroNaAgua = 0;
    
    @Override
    public void paint(Graphics celula) {
       
        contadorAnimacaoTiroNaAgua++; 
        if (contadorAnimacaoTiroNaAgua == 5) {
            contadorAnimacaoTiroNaAgua = 1;
        } 
        TiroNaAgua tiroNaAgua = new TiroNaAgua(contadorAnimacaoTiroNaAgua);

        for(int i = 0; i < Mapa.LINHAS; i++) {
            for(int j = 0; j < Mapa.COLUNAS; j++) {
                if(matrizTiro[i][j] == 2) {
                    celula.drawImage(tiroNaAgua.getTiroNaAgua(), i*LARGURA_CELULA, j*ALTURA_CELULA, LARGURA_CELULA, ALTURA_CELULA, null);
                }
            }
        }
    }

   public void setShot(int x, int y) {
 
        if (mapa.getMatrizMapa()[y][x] == 0) {
            matrizTiro[x][y] = 2;
        }    
    } 
}