
package model;

import java.awt.Image;
import java.io.IOException;
import java.util.logging.Level;
import java.util.logging.Logger;

public class Explosao {
    public Image explosao ;
    
    public Explosao(int contadorAnimacaoExplosao) { 
        
        try {
            explosao = javax.imageio.ImageIO.read(new java.net.URL(getClass().getResource("/imagens/explosao/explosion"+contadorAnimacaoExplosao+".png"), "explosion"+contadorAnimacaoExplosao+".png"));
        } catch (IOException ex) {
            Logger.getLogger(Oceano.class.getName()).log(Level.SEVERE, null, ex);
        }       
    }

    public Image getExplosao() {
        return explosao;
    }    
}
