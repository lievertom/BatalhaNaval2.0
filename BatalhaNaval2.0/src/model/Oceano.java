
package model;

import java.awt.Image;
import java.io.IOException;
import java.util.logging.Level;
import java.util.logging.Logger;

public class Oceano {
    private Image oceano;
    
    public Oceano(int contadorAnimacaoOceano) { 
        try {
            oceano = javax.imageio.ImageIO.read(new java.net.URL(getClass().getResource("/imagens/oceano/ocean"+contadorAnimacaoOceano+".png"), "ocean"+contadorAnimacaoOceano+".png"));
        } catch (IOException ex) {
            Logger.getLogger(Oceano.class.getName()).log(Level.SEVERE, null, ex);
        }       
    }

    public Image getOceano() {
        return oceano;
    }
    
}
