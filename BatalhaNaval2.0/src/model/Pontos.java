
package model;

import javax.swing.JLabel;

public class Pontos extends JLabel{
    private int  pontos = 0;  
    
    public int getPontos() {
        return pontos;
    }

    public void setPontos(int pontos) {
        this.pontos = pontos;
    }
    
}
