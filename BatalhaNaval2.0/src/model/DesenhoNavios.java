package model;

import java.awt.Graphics;
import java.util.Random;
import javax.swing.JLabel;
import view.MenuPrincipal;

public class DesenhoNavios extends JLabel {
    public static final int ALTURA_CELULA = 70;
    public static final int LARGURA_CELULA = 70;
    private boolean fimDeJogo = false;
    private int barco = 1;
    private int barco2 = 2;
    private int barco3 = 3;
    private int barco4 = 4;
    private int barco5 = 5;
    public static int pontos = 0;
    Random random = new Random();
    final Mapa mapa = new Mapa(MenuPrincipal.NUMERO_MAPA);
    private int [][] matrizNavios = mapa.getMatrizPosicaoNavios();

    @Override
    public void paint(Graphics celula) {
       if(barco == 0) {
            Navio navio = new Navio("Patrol");
            celula.drawImage(Navio.NAVIO, mapa.getColuna()[0]*LARGURA_CELULA+LARGURA_CELULA*3/8, mapa.getLinha()[0]*ALTURA_CELULA, LARGURA_CELULA/4, ALTURA_CELULA, null);
        }
        if(barco2 == 0) {                    
            if (mapa.getMatrizMapa()[mapa.getLinha()[1]+1][mapa.getColuna()[1]] == 2) {
                Navio navio = new Navio("Cruiser");
                celula.drawImage(Navio.NAVIO, mapa.getColuna()[1]*LARGURA_CELULA+LARGURA_CELULA/3, mapa.getLinha()[1]*ALTURA_CELULA, LARGURA_CELULA/3, ALTURA_CELULA*2, null);
            }
            else {
                Navio navio = new Navio("Cruiser2");
                celula.drawImage(Navio.NAVIO, mapa.getColuna()[1]*LARGURA_CELULA, mapa.getLinha()[1]*ALTURA_CELULA+ALTURA_CELULA/3, LARGURA_CELULA*2, ALTURA_CELULA/3, null);
            }
        }
        if (barco3 == 0) {                   
            if (mapa.getMatrizMapa()[mapa.getLinha()[2]+1][mapa.getColuna()[2]] == 3) {
                Navio navio = new Navio("Destroyer");
                celula.drawImage(Navio.NAVIO, mapa.getColuna()[2]*LARGURA_CELULA+LARGURA_CELULA/3, mapa.getLinha()[2]*ALTURA_CELULA, LARGURA_CELULA/3, ALTURA_CELULA*3, null);
            }
            else {
                Navio navio = new Navio("Destroyer2");
                celula.drawImage(Navio.NAVIO, mapa.getColuna()[2]*LARGURA_CELULA, mapa.getLinha()[2]*ALTURA_CELULA+ALTURA_CELULA/3, LARGURA_CELULA*3, ALTURA_CELULA/3, null);
            }
        }
        if(barco4 == 0) {                   
            if (mapa.getMatrizMapa()[mapa.getLinha()[3]+1][mapa.getColuna()[3]] == 4) {
                Navio navio = new Navio("Carrier");
                celula.drawImage(Navio.NAVIO, mapa.getColuna()[3]*LARGURA_CELULA, mapa.getLinha()[3]*ALTURA_CELULA, LARGURA_CELULA, ALTURA_CELULA*4, null);
            }
            else {
                Navio navio = new Navio("Carrier2");
                celula.drawImage(Navio.NAVIO, mapa.getColuna()[3]*LARGURA_CELULA, mapa.getLinha()[3]*ALTURA_CELULA, LARGURA_CELULA*4, ALTURA_CELULA, null);
            }
        }
        if(barco5 == 0) {                    
            if (mapa.getMatrizMapa()[mapa.getLinha()[4]+1][mapa.getColuna()[4]] == 5) {
                Navio navio = new Navio("Battleship");
                celula.drawImage(Navio.NAVIO, mapa.getColuna()[4]*LARGURA_CELULA+LARGURA_CELULA/4, mapa.getLinha()[4]*ALTURA_CELULA, LARGURA_CELULA/2, ALTURA_CELULA*5, null);
            }
            else {
                Navio navio = new Navio("Battleship2");
                celula.drawImage(Navio.NAVIO, mapa.getColuna()[4]*LARGURA_CELULA, mapa.getLinha()[4]*ALTURA_CELULA+LARGURA_CELULA/4, LARGURA_CELULA*5, ALTURA_CELULA/2, null);
            }
        }
        
    }
    
    public void setShot(int x, int y) {
 
        if(mapa.getMatrizMapa()[y][x] > 0) {
            if(mapa.getMatrizMapa()[y][x] == 1) {
                barco--;
                pontos += 100;
            }
            else if(mapa.getMatrizMapa()[y][x] == 2) {
                barco2 --;
                pontos += 100;
                if (barco2 == 0) {
                    pontos += 100;
                }
            }
            else if(mapa.getMatrizMapa()[y][x] == 3) {
                barco3--;
                pontos += 100;
                if (barco3 == 0){
                    pontos += 100;
                }
            }
            else if(mapa.getMatrizMapa()[y][x] == 4) {
                barco4--;
                pontos += 100;  
                if (barco4 == 0){
                    pontos += 100;
                }
            }
            else if(mapa.getMatrizMapa()[y][x] == 5) {
                barco5--;
                pontos += 100;
                if (barco5 == 0){
                    pontos += 100;
                }
            }
        }
    }
    
    public boolean FimDejogo () {
        if(barco == 0 && barco2 == 0 && barco3 == 0 && barco4 == 0 && barco5 == 0){
            pontos += 2000; 
            fimDeJogo = true; 
        }
        return fimDeJogo;
    }
    
    public int getPontos() {
        return pontos;
    }
}