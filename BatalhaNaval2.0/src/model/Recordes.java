
package model;

import java.io.BufferedReader;
import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.FileWriter;
import java.io.IOException;
import java.io.PrintWriter;
import java.util.logging.Level;
import java.util.logging.Logger;

public class Recordes {
    
    private static int QUANTIDADE_RANQUEADOS = 5;
    
    private String listaJogador[];
    private String jogador[];
    private int listaPontos[];
    private int pontos[];
    private int menorPontuacao;
    private String nome; 
    private String lerLinha;
    private  FileReader arquivo;
    private BufferedReader lerArquivo;
    
    public Recordes() {
        listaJogador = new String[QUANTIDADE_RANQUEADOS];
        jogador = new String[QUANTIDADE_RANQUEADOS];
        listaPontos = new int[QUANTIDADE_RANQUEADOS];
        pontos = new int[QUANTIDADE_RANQUEADOS];
        try {
            arquivo = new FileReader("src/arquivostxt/recordes.txt");            
            lerArquivo = new BufferedReader(arquivo);
            for(int i = 0; i < QUANTIDADE_RANQUEADOS; i++) {
                try {
                    lerLinha = lerArquivo.readLine();
                    listaJogador[i] = lerLinha;
                    lerLinha = lerArquivo.readLine();
                    listaPontos[i] = Integer.valueOf(lerLinha);
                } catch (IOException ex) {
                    Logger.getLogger(Recordes.class.getName()).log(Level.SEVERE, null, ex);
                }       
            }
        } catch (FileNotFoundException ex) {
            Logger.getLogger(Recordes.class.getName()).log(Level.SEVERE, null, ex);
        }
        for(int i = 0; i < QUANTIDADE_RANQUEADOS; i++) {
            for(int j = 0; j < QUANTIDADE_RANQUEADOS; j++) {
                if(listaPontos[i] > listaPontos[j]) {
                    menorPontuacao = listaPontos[i]; 
                    listaPontos[i] = listaPontos[j];
                    listaPontos[j] = menorPontuacao;
                    nome = listaJogador[i];
                    listaJogador[i] = listaJogador[j];
                    listaJogador[j] = nome;
                }
            }
        }
        pontos = listaPontos;
        jogador = listaJogador;
    } 
    
    public void salvarRecorde(String jogador4, int pontos4) {
    	PrintWriter out;
        try {
            out = new PrintWriter(new FileWriter("src/arquivostxt/recordes.txt"));
            for(int i = 0; i < QUANTIDADE_RANQUEADOS - 1; i++) {
                out.write(jogador[i]+"\n");
                out.write(pontos[i]+"\n");
            }
            out.write(jogador4+"\n");
            out.write(pontos4+"\n");
            out.close();
        } catch (IOException ex) {
            Logger.getLogger(Recordes.class.getName()).log(Level.SEVERE, null, ex);
        }			
    }
    public String[] getJogador() {
        return jogador;
    }

    public void setJogador(String[] jogador) {
        this.jogador = jogador;
    }

    public int[] getPontos() {
        return pontos;
    }

    public void setPontos(int[] pontos) {
        this.pontos = pontos;
    }
    
}
