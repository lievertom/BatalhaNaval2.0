
package model;

import java.awt.Graphics;

public class DesenhoExplosao extends DesenhoNavios {
    public static final int ALTURA_CELULA = 70;
    public static final int LARGURA_CELULA = 70;
    private int [][] matrizTiro = new int[Mapa.LINHAS][Mapa.COLUNAS];
 
    private int contadorAnimacaoExplosao = 0;
    
    @Override
    public void paint(Graphics celula) {
       
        contadorAnimacaoExplosao++; 
        if (contadorAnimacaoExplosao == 6) {
            contadorAnimacaoExplosao = 1;
        } 
        Explosao explosao = new Explosao(contadorAnimacaoExplosao);

        for(int i = 0; i < Mapa.LINHAS; i++) {
            for(int j = 0; j < Mapa.COLUNAS; j++) {
                if(matrizTiro[i][j] == 1) {
                    celula.drawImage(explosao.getExplosao(), i*LARGURA_CELULA, j*ALTURA_CELULA, LARGURA_CELULA, ALTURA_CELULA, null);
                }
            }
        }
    }

   public void setShot(int x, int y) {
 
        if (mapa.getMatrizMapa()[y][x] > 0) {
            matrizTiro[x][y] = 1;
        }    
    } 
}
