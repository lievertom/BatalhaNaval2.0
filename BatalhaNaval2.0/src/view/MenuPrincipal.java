
package view;

import java.beans.PropertyVetoException;
import java.util.Random;
import java.util.logging.Level;
import java.util.logging.Logger;
import model.Mapa;

public class MenuPrincipal extends javax.swing.JFrame {

    private TelaDeFundo telaDeFundo = new TelaDeFundo();
    public static int NUMERO_MAPA;
    
    public MenuPrincipal() {
        initComponents();
        setTitle("Menu Principal");
        areaDeTrabalho.add(telaDeFundo);
        
        try {
            telaDeFundo.setMaximum(true);
        } catch (PropertyVetoException ex) {
            Logger.getLogger(MenuPrincipal.class.getName()).log(Level.SEVERE, null, ex);
        }
            telaDeFundo.setVisible(true);      
        
    }

    @SuppressWarnings("unchecked")
    // <editor-fold defaultstate="collapsed" desc="Generated Code">//GEN-BEGIN:initComponents
    private void initComponents() {

        areaDeTrabalho = new javax.swing.JDesktopPane();
        jogar = new javax.swing.JButton();
        instrucoes = new javax.swing.JButton();
        recordes = new javax.swing.JButton();

        setDefaultCloseOperation(javax.swing.WindowConstants.EXIT_ON_CLOSE);
        setMinimumSize(new java.awt.Dimension(550, 400));
        getContentPane().setLayout(new java.awt.CardLayout());

        areaDeTrabalho.setMinimumSize(new java.awt.Dimension(550, 400));

        jogar.setFont(new java.awt.Font("Lucida Handwriting", 1, 18)); // NOI18N
        jogar.setText("jogar");
        jogar.setCursor(new java.awt.Cursor(java.awt.Cursor.HAND_CURSOR));
        jogar.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                jogarActionPerformed(evt);
            }
        });

        instrucoes.setFont(new java.awt.Font("Lucida Handwriting", 1, 18)); // NOI18N
        instrucoes.setText("instruções");
        instrucoes.setCursor(new java.awt.Cursor(java.awt.Cursor.HAND_CURSOR));
        instrucoes.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                instrucoesActionPerformed(evt);
            }
        });

        recordes.setFont(new java.awt.Font("Lucida Handwriting", 1, 18)); // NOI18N
        recordes.setText("recordes");
        recordes.setCursor(new java.awt.Cursor(java.awt.Cursor.HAND_CURSOR));
        recordes.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                recordesActionPerformed(evt);
            }
        });

        areaDeTrabalho.setLayer(jogar, javax.swing.JLayeredPane.PALETTE_LAYER);
        areaDeTrabalho.setLayer(instrucoes, javax.swing.JLayeredPane.PALETTE_LAYER);
        areaDeTrabalho.setLayer(recordes, javax.swing.JLayeredPane.PALETTE_LAYER);

        javax.swing.GroupLayout areaDeTrabalhoLayout = new javax.swing.GroupLayout(areaDeTrabalho);
        areaDeTrabalho.setLayout(areaDeTrabalhoLayout);
        areaDeTrabalhoLayout.setHorizontalGroup(
            areaDeTrabalhoLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(javax.swing.GroupLayout.Alignment.TRAILING, areaDeTrabalhoLayout.createSequentialGroup()
                .addContainerGap(javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                .addGroup(areaDeTrabalhoLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.TRAILING)
                    .addComponent(recordes, javax.swing.GroupLayout.PREFERRED_SIZE, 156, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(instrucoes, javax.swing.GroupLayout.PREFERRED_SIZE, 156, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(jogar, javax.swing.GroupLayout.PREFERRED_SIZE, 156, javax.swing.GroupLayout.PREFERRED_SIZE))
                .addGap(66, 66, 66))
        );
        areaDeTrabalhoLayout.setVerticalGroup(
            areaDeTrabalhoLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(areaDeTrabalhoLayout.createSequentialGroup()
                .addGap(128, 128, 128)
                .addComponent(jogar, javax.swing.GroupLayout.PREFERRED_SIZE, 44, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addGap(18, 18, 18)
                .addComponent(instrucoes, javax.swing.GroupLayout.PREFERRED_SIZE, 44, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addGap(18, 18, 18)
                .addComponent(recordes, javax.swing.GroupLayout.PREFERRED_SIZE, 44, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addContainerGap(javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))
        );

        getContentPane().add(areaDeTrabalho, "card2");

        setBounds(400, 200, 499, 411);
    }// </editor-fold>//GEN-END:initComponents

    private void instrucoesActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_instrucoesActionPerformed
        TelaDeInstrucoes telaDeInstrucoes = new TelaDeInstrucoes();
        telaDeInstrucoes.setVisible(true);
    }//GEN-LAST:event_instrucoesActionPerformed

    private void recordesActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_recordesActionPerformed
        TelaDeRecordes telaDeRecordes = new TelaDeRecordes();
        telaDeRecordes.setVisible(true);
    }//GEN-LAST:event_recordesActionPerformed

    private void jogarActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_jogarActionPerformed
        TelaDeJogo telaDoJogo = new TelaDeJogo();
        telaDoJogo.setVisible(true);
        Random random = new Random();
        NUMERO_MAPA = random.nextInt(Mapa.NUMERO_DE_MAPAS);
        final Mapa mapa = new Mapa(NUMERO_MAPA);
    }//GEN-LAST:event_jogarActionPerformed

    /**
     * @param args the command line arguments
     */
    public static void main(String args[]) {
        /* Set the Nimbus look and feel */
        //<editor-fold defaultstate="collapsed" desc=" Look and feel setting code (optional) ">
        /* If Nimbus (introduced in Java SE 6) is not available, stay with the default look and feel.
         * For details see http://download.oracle.com/javase/tutorial/uiswing/lookandfeel/plaf.html 
         */
        try {
            for (javax.swing.UIManager.LookAndFeelInfo info : javax.swing.UIManager.getInstalledLookAndFeels()) {
                if ("Nimbus".equals(info.getName())) {
                    javax.swing.UIManager.setLookAndFeel(info.getClassName());
                    break;
                }
            }
        } catch (ClassNotFoundException ex) {
            java.util.logging.Logger.getLogger(MenuPrincipal.class.getName()).log(java.util.logging.Level.SEVERE, null, ex);
        } catch (InstantiationException ex) {
            java.util.logging.Logger.getLogger(MenuPrincipal.class.getName()).log(java.util.logging.Level.SEVERE, null, ex);
        } catch (IllegalAccessException ex) {
            java.util.logging.Logger.getLogger(MenuPrincipal.class.getName()).log(java.util.logging.Level.SEVERE, null, ex);
        } catch (javax.swing.UnsupportedLookAndFeelException ex) {
            java.util.logging.Logger.getLogger(MenuPrincipal.class.getName()).log(java.util.logging.Level.SEVERE, null, ex);
        }
        //</editor-fold>

        /* Create and display the form */
        java.awt.EventQueue.invokeLater(new Runnable() {
            public void run() {
                new MenuPrincipal().setVisible(true);
            }
        });
    }

    // Variables declaration - do not modify//GEN-BEGIN:variables
    private javax.swing.JDesktopPane areaDeTrabalho;
    private javax.swing.JButton instrucoes;
    private javax.swing.JButton jogar;
    private javax.swing.JButton recordes;
    // End of variables declaration//GEN-END:variables
}
