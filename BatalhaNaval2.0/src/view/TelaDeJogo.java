
package view;

import model.DesenhoExplosao;
import controller.RenderizarExplosao;
import controller.RenderizarNavios;
import controller.RenderizarOceano;
import controller.RenderizarPontos;
import controller.RenderizarTiroNaAgua;
import java.awt.Color;
import java.awt.event.MouseEvent;
import java.awt.event.MouseListener;
import model.DesenhoNavios;
import model.DesenhoOceano;
import model.DesenhoTiroNaAgua;
import model.Mapa;
import model.Pontos;


public class TelaDeJogo extends javax.swing.JFrame {

    private int tiro1 = 15;
    private int tiro2 = 1;
    private int tiro3 = 1;
    private int tiro4 = 1;
    private boolean fimDeJogo;
    private DesenhoExplosao canvasExplosao = new DesenhoExplosao();
    private DesenhoNavios canvasNavios = new DesenhoNavios();
    private DesenhoTiroNaAgua canvasTiroNaAgua = new DesenhoTiroNaAgua();
    private final DesenhoOceano canvasOceano = new DesenhoOceano();
    private final  Pontos pontos = new Pontos();
    private final RenderizarPontos renderizarPontos = new RenderizarPontos(pontos);
    private final RenderizarExplosao renderizarExplosao = new RenderizarExplosao(canvasExplosao);
    private final RenderizarNavios renderizarNavios = new RenderizarNavios(canvasNavios);
    private final RenderizarTiroNaAgua renderizarTiroNaAgua = new RenderizarTiroNaAgua(canvasTiroNaAgua);
    private final RenderizarOceano rendenrizarOceano = new RenderizarOceano(canvasOceano);
    private boolean [][] matrizEscolha = Mapa.MATRIZ_ESCOLHA;
       
    public TelaDeJogo() {
        initComponents();
        
        setDefaultCloseOperation(javax.swing.WindowConstants.HIDE_ON_CLOSE);
        setBounds(250, 50, 740, 640);
        setTitle("Batalha Naval 2.0 ");
        setVisible(true);
        
        getContentPane().setLayout(new org.netbeans.lib.awtextra.AbsoluteLayout());
        getContentPane().add(pontos, new org.netbeans.lib.awtextra.AbsoluteConstraints(20, 50, 100, 30));
        getContentPane().add(canvasExplosao, new org.netbeans.lib.awtextra.AbsoluteConstraints(140, 20, 560, 560));
        getContentPane().add(canvasNavios, new org.netbeans.lib.awtextra.AbsoluteConstraints(140, 20, 560, 560));
        getContentPane().add(canvasTiroNaAgua, new org.netbeans.lib.awtextra.AbsoluteConstraints(140, 20, 560, 560));
        getContentPane().add(canvasOceano, new org.netbeans.lib.awtextra.AbsoluteConstraints(140, 20, 560, 560));
        getContentPane().setBackground(Color.black);
                
        arma1.setBackground(Color.blue);
        
        pontos.setForeground(Color.blue);
        pontos.setFont(new java.awt.Font("Lucida Handwriting", 3, 24));
       
        renderizarPontos.start();
        renderizarNavios.start();
        renderizarExplosao.start();        
        renderizarTiroNaAgua.start();
        rendenrizarOceano.start();

        canvasNavios.addMouseListener(new MouseListener() {

            @Override
            public void mouseReleased(MouseEvent e) {
                int x=e.getX();
                int y=e.getY();

                int x_pos = x/DesenhoNavios.LARGURA_CELULA;
                int y_pos = y/DesenhoNavios.ALTURA_CELULA;
                if (tiro2 == 2) {
                    for(int i = 0; i < Mapa.LINHAS; i++) {
                        if (matrizEscolha[i][x_pos] == true) {
                            canvasExplosao.setShot(x_pos, i);
                            canvasNavios.setShot(x_pos, i);
                            canvasTiroNaAgua.setShot(x_pos, i);
                            matrizEscolha[i][x_pos] = false; 
                        }
                    }
                    tiro2 = 0;
                    arma2.setBackground(Color.red);
                }if (tiro3 == 2) {
                    for(int i = 0; i < Mapa.COLUNAS; i++) {
                        if (matrizEscolha[y_pos][i] == true) {
                            canvasExplosao.setShot(i, y_pos);
                            canvasNavios.setShot(i, y_pos);
                            canvasTiroNaAgua.setShot(i, y_pos);
                            matrizEscolha[y_pos][i] = false; 
                        }
                    }
                    tiro3 = 0;
                    arma3.setBackground(Color.red);
                    
                }
                try {
                    if (tiro4 == 2) {
                        for(int i = -1; i < 2; i++) {
                            for(int j = -1; j < 2; j++) {
                                if (matrizEscolha[i+y_pos][j+x_pos] == true) {
                                    canvasExplosao.setShot(j+x_pos, i+y_pos);
                                    canvasNavios.setShot(j+x_pos, i+y_pos);
                                    canvasTiroNaAgua.setShot(j+x_pos, i+y_pos);
                                    matrizEscolha[i+y_pos][j+x_pos] = false; 
                                }
                            }
                        }
                        tiro4 = 0;
                        arma4.setBackground(Color.red);
                    }
                }
                catch(IllegalMonitorStateException exception) {
                    exception.notify();
                }
                if (matrizEscolha[y_pos][x_pos] == true && tiro1 > 0) {                    
                    canvasExplosao.setShot(x_pos, y_pos);
                    canvasNavios.setShot(x_pos, y_pos);
                    canvasTiroNaAgua.setShot(x_pos, y_pos);
                    matrizEscolha[y_pos][x_pos] = false;
                    tiro1--;
                    if(tiro1 == 0) {
                        arma1.setBackground(Color.red);
                    }
                }
                if(tiro1 != 0) {
                    arma1.setBackground(Color.blue);
                }
                fimDeJogo = canvasNavios.FimDejogo();
                if(tiro1 == 0 && tiro2 == 0 && tiro3 == 0 && tiro4 == 0) {
                    fimDeJogo = true;
                }
                if(fimDeJogo == true){                    
                    FimDeJogo fimDeJogo = new FimDeJogo();
                    fimDeJogo.setVisible(true);
                    fimDeJogo.setPontos(DesenhoNavios.pontos);
                    DesenhoNavios.pontos = 0;
                    dispose();
                }
            }
            
            @Override
            public void mouseClicked(MouseEvent e) {
            }

            @Override
            public void mousePressed(MouseEvent e) {
            }

            @Override
            public void mouseEntered(MouseEvent e) {
            }

            @Override
            public void mouseExited(MouseEvent e) {
            }


        });
    }
    
    @SuppressWarnings("unchecked")
    // <editor-fold defaultstate="collapsed" desc="Generated Code">//GEN-BEGIN:initComponents
    private void initComponents() {

        arma4 = new javax.swing.JButton();
        arma2 = new javax.swing.JButton();
        arma1 = new javax.swing.JButton();
        pontuacao = new javax.swing.JLabel();
        arma3 = new javax.swing.JButton();

        setResizable(false);

        arma4.setIcon(new javax.swing.ImageIcon(getClass().getResource("/imagens/armas/WeaponBattleshipStandardGun.png"))); // NOI18N
        arma4.setMaximumSize(new java.awt.Dimension(70, 70));
        arma4.setMinimumSize(new java.awt.Dimension(70, 70));
        arma4.setPreferredSize(new java.awt.Dimension(70, 70));
        arma4.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                arma4ActionPerformed(evt);
            }
        });

        arma2.setIcon(new javax.swing.ImageIcon(getClass().getResource("/imagens/armas/WeaponSubmarineStandard.png"))); // NOI18N
        arma2.setPreferredSize(new java.awt.Dimension(70, 70));
        arma2.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                arma2ActionPerformed(evt);
            }
        });

        arma1.setIcon(new javax.swing.ImageIcon(getClass().getResource("/imagens/armas/Missile.png"))); // NOI18N
        arma1.setPreferredSize(new java.awt.Dimension(70, 70));
        arma1.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                arma1ActionPerformed(evt);
            }
        });

        pontuacao.setFont(new java.awt.Font("Lucida Handwriting", 3, 14)); // NOI18N
        pontuacao.setForeground(new java.awt.Color(0, 51, 102));
        pontuacao.setText("Pontuação:");

        arma3.setIcon(new javax.swing.ImageIcon(getClass().getResource("/imagens/armas/WeaponDestroyerStandardGun.png"))); // NOI18N
        arma3.setPreferredSize(new java.awt.Dimension(70, 70));
        arma3.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                arma3ActionPerformed(evt);
            }
        });

        javax.swing.GroupLayout layout = new javax.swing.GroupLayout(getContentPane());
        getContentPane().setLayout(layout);
        layout.setHorizontalGroup(
            layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(layout.createSequentialGroup()
                .addGap(19, 19, 19)
                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addComponent(arma3, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(arma4, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.TRAILING, false)
                        .addComponent(arma2, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                        .addComponent(arma1, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))
                    .addComponent(pontuacao, javax.swing.GroupLayout.PREFERRED_SIZE, 110, javax.swing.GroupLayout.PREFERRED_SIZE))
                .addContainerGap(469, Short.MAX_VALUE))
        );
        layout.setVerticalGroup(
            layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(layout.createSequentialGroup()
                .addGap(25, 25, 25)
                .addComponent(pontuacao)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED, 94, Short.MAX_VALUE)
                .addComponent(arma4, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addGap(18, 18, 18)
                .addComponent(arma3, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addGap(18, 18, 18)
                .addComponent(arma2, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addGap(18, 18, 18)
                .addComponent(arma1, javax.swing.GroupLayout.PREFERRED_SIZE, 69, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addGap(23, 23, 23))
        );

        pack();
    }// </editor-fold>//GEN-END:initComponents

    private void arma4ActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_arma4ActionPerformed
        if (tiro4 == 1) {
            tiro4 = 2;
            arma4.setBackground(Color.blue);
            if (tiro1 != 0) {
                arma1.setBackground(new Color(240, 240, 240));
            }
        }
        else if (tiro4 == 2) {
            tiro4 = 1;
            arma4.setBackground(new Color(240, 240, 240));
            if (tiro2 != 2 && tiro3 != 2 && tiro1 != 0) {
                arma1.setBackground(Color.blue);
            }
        }
    }//GEN-LAST:event_arma4ActionPerformed

    private void arma1ActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_arma1ActionPerformed
    
    }//GEN-LAST:event_arma1ActionPerformed

    private void arma2ActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_arma2ActionPerformed
        if (tiro2 == 1) {
            tiro2 = 2;
            arma2.setBackground(Color.blue);
            if (tiro1 != 0) {
                arma1.setBackground(new Color(240, 240, 240));
            }
        }
        else if (tiro2 == 2) {
            tiro2 = 1;
            arma2.setBackground(new Color(240, 240, 240));
            if (tiro4 != 2 && tiro3 != 2 && tiro1 != 0) {
                arma1.setBackground(Color.blue);
            }
        }
    }//GEN-LAST:event_arma2ActionPerformed

    private void arma3ActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_arma3ActionPerformed
        if (tiro3 == 1) {
            tiro3 = 2;
            arma3.setBackground(Color.blue);
            if (tiro1 != 0) {
                arma1.setBackground(new Color(240, 240, 240));
            }
        }
        else if (tiro3 == 2) {
            tiro3 = 1;
            arma3.setBackground(new Color(240, 240, 240));
            if (tiro2 != 2 && tiro4 != 2 && tiro1 != 0) {
                arma1.setBackground(Color.blue);
            }
        }
    }//GEN-LAST:event_arma3ActionPerformed

    public static void main(String args[]) {
        /* Set the Nimbus look and feel */
        //<editor-fold defaultstate="collapsed" desc=" Look and feel setting code (optional) ">
        /* If Nimbus (introduced in Java SE 6) is not available, stay with the default look and feel.
         * For details see http://download.oracle.com/javase/tutorial/uiswing/lookandfeel/plaf.html 
         */
        try {
            for (javax.swing.UIManager.LookAndFeelInfo info : javax.swing.UIManager.getInstalledLookAndFeels()) {
                if ("Nimbus".equals(info.getName())) {
                    javax.swing.UIManager.setLookAndFeel(info.getClassName());
                    break;
                }
            }
        } catch (ClassNotFoundException | InstantiationException | IllegalAccessException | javax.swing.UnsupportedLookAndFeelException ex) {
            java.util.logging.Logger.getLogger(TelaDeJogo.class.getName()).log(java.util.logging.Level.SEVERE, null, ex);
        }
        //</editor-fold>
        //</editor-fold>
        
        //</editor-fold>

        /* Create and display the form */
        java.awt.EventQueue.invokeLater(new Runnable() {
            public void run() {
                new TelaDeJogo().setVisible(true);
            }
        });
    }

    // Variables declaration - do not modify//GEN-BEGIN:variables
    private javax.swing.JButton arma1;
    private javax.swing.JButton arma2;
    private javax.swing.JButton arma3;
    private javax.swing.JButton arma4;
    private javax.swing.JLabel pontuacao;
    // End of variables declaration//GEN-END:variables
}
